const express = require('express');

const path = require('path');
const fs = require('fs').promises;

const cors = require('cors');
const morgan = require('morgan');

const app = express();

app.use(cors());
app.use(express.urlencoded());
app.use(express.json());
app.use(morgan('dev'));

const createFile = (res, data) => {
  fs.writeFile(path.join('files', data.filename), data.content)
    .then(() => {
      res.json({ message: `File ${data.filename} created successfully` }, 200);
    })
    .catch((err) => {
      res.json({ message: err.code }, 500);
    });
};
app.post('/api/files', (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content || '';

  if (filename && /\.(log|txt|json|yaml|xml|js)$/i.test(filename)) {
    fs.access('files')
      .then(() => {
        createFile(res, { filename: filename, content: content });
      })
      .catch((err) => {
        if (err.code == 'ENOENT') {
          fs.mkdir(path.join(__dirname, 'files'))
            .then(() => {
              createFile(res, { filename: filename, content: content });
            })
            .catch((err) => {
              res.join({ message: err.code }, 500);
            });
        } else {
          res.json({ message: err.code }, 500);
        }
      });
  } else {
    if (!filename) {
      res.json({ message: "Please specify 'filename' parameter" }, 400);
    } else {
      res.json({ message: 'Wrong file extension' }, 400);
    }
  }
});

app.get('/api/files', (req, res) => {
  fs.readdir(path.join(__dirname, 'files'))
    .then((files) => {
      res.json(
        {
          message: 'Success',
          files: files,
        },
        200
      );
    })
    .catch((err) => {
      if (err.code == 'ENOENT') {
        fs.mkdir(path.join(__dirname, 'files'))
          .then(() => {
            res.json(
              {
                message: 'Success',
                files: [],
              },
              200
            );
          })
          .catch((err) => {
            res.json({ message: err.code }, 500);
          });
      } else {
        res.json({ message: err.code }, 500);
      }
    });
});

app.get('/api/files/:filename', (req, res) => {
  const filename = req.params.filename;
  if (/\.(log|txt|json|yaml|xml|js)$/i.test(filename)) {
    const filepath = path.join(__dirname, 'files', filename);
    fs.readFile(filepath, 'utf8')
      .then((filedata) => {
        fs.stat(filepath)
          .then((stats) => {
            res.json(
              {
                message: 'Success',
                filename: filename,
                content: filedata,
                extension: path.extname(req.params.filename).slice(1),
                uploadedDate: stats.birthtime,
              },
              200
            );
          })
          .catch((err) => {
            res.json({ message: err.code }, 500);
          });
      })
      .catch((err) => {
        if (err.code == 'ENOENT') {
          res.json({ message: `File ${filename} does not exist` }, 400);
        } else {
          res.json({ message: err.code }, 500);
        }
      });
  } else {
    res.json({ message: 'Wrong file extension' }, 400);
  }
});

app.delete('/api/files', (req, res) => {
  const filename = req.body.filename;

  if (filename && /\.(log|txt|json|yaml|xml|js)$/i.test(filename)) {
    fs.access(path.join('files', filename))
      .then(() => {
        fs.unlink(path.join('files', filename))
          .then(() => {
            res.json({ message: `File ${filename} was deleted` }, 200);
          })
          .catch((err) => {
            res.json({ message: err.code }, 500);
          });
      })
      .catch((err) => {
        if (err.code == 'ENOENT') {
          res.json({ message: `File ${filename} does not exist` }, 400);
        } else {
          res.json({ message: err.code }, 500);
        }
      });
  } else {
    if (!filename) {
      res.json({ message: "Please specify 'filename' parameter" }, 400);
    } else {
      res.json({ message: 'Wrong file extension' }, 400);
    }
  }
});

app.put('/api/files', (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content;

  if (content && filename && /\.(log|txt|json|yaml|xml|js)$/i.test(filename)) {
    fs.access(path.join('files', filename))
      .then(() => {
        fs.appendFile(path.join('files', filename), content)
          .then(() => {
            res.json({ message: `Content was added to file ${filename}` }, 200);
          })
          .catch((err) => {
            res.json({ message: err.code }, 500);
          });
      })
      .catch((err) => {
        if (err.code == 'ENOENT') {
          res.json({ message: `File ${filename} does not exist` }, 400);
        } else {
          res.json({ message: err.code }, 500);
        }
      });
  } else {
    if (!filename) {
      res.json({ message: "Please specify 'filename' parameter" }, 400);
    } else if (!content) {
      res.json({ message: "Please specify 'content' parameter" }, 400);
    } else {
      res.json({ message: 'Wrong file extension' }, 400);
    }
  }
});

app.listen(8080);
